Sealed Secrets
===============================

Construct a YAML file of your desired Kubernetes Secret data:

```sh
$ cat matrix-system-db.yaml 

postgresql-password: "37dca6...69e63f7"
postgresql-postgres-password: "037e55d...b391fc746"
```

Generate a Kubernetes Secret manifest from one or more of these secret files:

```sh

$ python3 generate_secret.py --flat --name postgres-db --output postgres-db.secret.yaml postgres-db.yaml apiVersion: v1

$ cat postgres-db.secret.yaml

data:
  postgresql-password: MzdkY2E2...ZTYzZjc=
  postgresql-postgres-password: MDM3ZTU1ZD...kxZmM3NDY=
kind: Secret
metadata:
  name: matrix-synapse-db
type: Opaque

```

Encrypt the secret as a Sealed Secret suitable for committing to the deployment repo:

```sh

$ kubeseal --scope cluster-wide --controller-name=sealed-secrets --controller-namespace=sealed-secrets -o yaml  < postgres-db.secret.yaml > postgres-db.sealed-secret.yaml 

$ cat postgres-db.sealed-secret.yaml

apiVersion: bitnami.com/v1alpha1
kind: SealedSecret
metadata:
  annotations:
    sealedsecrets.bitnami.com/cluster-wide: "true"
  creationTimestamp: null
  name: matrix-synapse-db
spec:
  encryptedData:
    postgresql-password: AgCSJT...Y71AICw==
    postgresql-postgres-password: AgBwF6...sRA==
  template:
    data: null
    metadata:
      annotations:
        sealedsecrets.bitnami.com/cluster-wide: "true"
      creationTimestamp: null
      name: matrix-synapse-db
    type: Opaque

```
