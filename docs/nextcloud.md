# Nextcloud

Nextcloud is a fairly complex service to deploy, even using the official Helm chart. This document contains the extra information for deploying, installing, configuring, and maintaining the Nextcloud service.

## Installation

Installation requires that

1. there be only a single replica and that the liveness and 
2. readiness probes are effectively disabled.

Otherwise, there seems to be

1. a conflict between replicas running the initialization script, and
2. probe failures causing the containers to restart before the script is able to finish.

After installation the number of replicas can be set greater than 1.

## Post-installation actions and manual configuration

Open a terminal in a nextcloud server container and install the desired apps:

```bash
runuser -u www-data bash
php occ app:install sociallogin
php occ app:install groupquota
php occ app:install groupfolders
php occ app:install calendar
php occ app:install tasks
php occ app:install deck
php occ app:install contacts
php occ app:install notes
php occ app:install forms
php occ app:install polls
php occ app:install spreed
```

## Configuration of OpenID Connect Authentication using Keycloak

Configure [Social Login](https://cloud.circuses.ncsa.illinois.edu/settings/admin/sociallogin) using the `occ` command line tool. Open a terminal in a nextcloud server container and switch to the `www-data` user:

```bash
$ kubectl exec -it -n circuses-nextcloud circuses-nextcloud-67f45fd946-6q2nk -- bash
root@circuses-nextcloud-67f45fd946-6q2nk:/var/www/html# runuser -u www-data bash
www-data@circuses-nextcloud-67f45fd946-6q2nk:~/html$ php occ list
```

(optional) View current configuration by querying the Nextcloud database:

```bash
$ kubectl exec -it -n circuses-nextcloud nextcloud-db-0 -- bash

I have no name!@nextcloud-db-0:/$ mysql -u nextcloud -p nextcloud

MariaDB [nextcloud]> SELECT * FROM oc_appconfig WHERE appid='sociallogin'\G
```

Run the `occ` commands below to configure the app. Replace the `$KEYCLOAK_DOMAIN`, `$KEYCLOAK_REALM`, `$KEYCLOAK_CLIENT_ID` and `$KEYCLOAK_CLIENT_SECRET` variables with your values.

```bash
php occ config:app:set sociallogin restrict_users_wo_assigned_groups --value='1'
php occ config:app:set sociallogin no_prune_user_groups --value='1'
php occ config:app:set sociallogin hide_default_login --value='1'
php occ config:app:set sociallogin allow_login_connect --value='1'
php occ config:app:set sociallogin auto_create_groups --value='1'
php occ config:app:set sociallogin custom_providers --value='
{
   "custom_oidc" : [
      {
         "authorizeUrl" : "https://$KEYCLOAK_DOMAIN/auth/realms/$KEYCLOAK_REALM/protocol/openid-connect/auth",
         "clientId" : "$KEYCLOAK_CLIENT_ID",
         "clientSecret" : "$KEYCLOAK_CLIENT_SECRET",
         "defaultGroup" : "",
         "displayNameClaim" : "name",
         "groupMapping" : {
            "/circuses-admin" : "admin"
         },
         "groupsClaim" : "groups",
         "logoutUrl" : "",
         "name" : "keycloak_oidc",
         "scope" : "openid",
         "style" : "keycloak",
         "title" : "Keycloak",
         "tokenUrl" : "https://$KEYCLOAK_DOMAIN/auth/realms/$KEYCLOAK_REALM/protocol/openid-connect/token",
         "userInfoUrl" : "https://$KEYCLOAK_DOMAIN/auth/realms/$KEYCLOAK_REALM/protocol/openid-connect/userinfo"
      }
   ]
}
'
```

## Upgrading

Upgrading Nextcloud to new versions can be accomplished by modifying the chart values:

1. Change the image tag to the desired version.
2. Disable the probes.
3. Reduce the replicas value to 1.

After upgrading, revert changes 2 and 3.
