# Secret generator

First [install the `kubeseal` CLI tool](https://github.com/bitnami-labs/sealed-secrets#installation).

Create a `./secrets/secrets.yaml` file structured like the example below. Add the Kubernetes Secret names you want associated with each secret. For example

```yaml
secrets:
- name: discourse-admin
  data:
    discourse-password: "*****"
```

Run the bulk secrets generator to create Sealed Secret manifests for each secret in the specified file.

```sh
python3 seal_bulk_secrets.py --file ./secrets/secrets.yaml 
```
