import synapse
import os
import json
import sys

CONFIG = {
    # Admin account access token
    'auth_token': os.environ.get('MATRIX_ADMIN_API_TOKEN', ''),
    # Synapse server URL (e.g. https://matrix.musesframework.io)
    'server_url': os.environ.get('MATRIX_HOMESERVER_URL', ''),
    # Shared registration secret
    'shared_secret': os.environ.get('MATRIX_SHARED_SECRET', ''),
    # Bot account access token
    'access_token': os.environ.get('MATRIX_ACCESS_TOKEN', ''),
    # Target Matrix room ID
    'room_id': os.environ.get('MATRIX_ROOM_ID', ''),
}



if __name__ == "__main__":
    api = synapse.SynapseAdminApi(CONFIG)

    '''
    Create a bot account:
    '''
    # username = ''
    # displayname = ''
    # password = ''
    # print(json.dumps(
    #     api.create_new_user(
    #         username=username,
    #         displayname=displayname,
    #         password=password,
    #     ),
    #     indent=2)
    # )

    '''
    Get user account info:
    '''
    # get_users = api.get_all_users()
    # users = sorted(get_users['users'], key=lambda user: user['creation_ts'])
    # for user in users:
    #     print(json.dumps(user, indent=2))

    # print(json.dumps(
    #     api.user_account_info(user_id="@discourse-bot:musesframework.io"),
    #     indent=2)
    # )

    '''
    Create a Matrix room and invite the bot to the room using a Matrix
    client like Element. Get the room ID from the room settings.

    Acquire the bot account access token from the Synapse database:

        $ kubectl exec -n matrix -it matrix-postgresql-0 -- bash
        I have no name!@matrix-postgresql-0:/$ export PGPASSWORD="$POSTGRES_PASSWORD"
        I have no name!@matrix-postgresql-0:/$ psql -U $POSTGRES_USER $POSTGRES_DB
        psql (11.14)
        synapse=> select * from public.access_tokens;

    Use this access token to accept the invitation on behalf of the bot:
    '''
    # print(json.dumps(
    #     api.accept_invitation(
    #         room_id=CONFIG['room_id']),
    #         access_token=CONFIG['access_token']),
    #     indent=2)
    # )

    # api.send_message(
    #     message_body='testing 1, 2, 3, testing',
    #     room_id=CONFIG['room_id']),
    #     access_token=CONFIG['access_token'],
    # )

    sys.exit()
